<?php

declare(strict_types=1);

namespace backendVkIntegration\infrastructure\operations;

use backendIntegrationCore\domain\collections\HeaderCollection;
use backendIntegrationCore\infrastructure\operations\AbstractApiOperation;
use backendVkIntegration\domain\dto\operations\GetStreamDTO;
use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;
use Throwable;

final class GetStreamOperation extends AbstractApiOperation
{
    
    private ?string $downloadUrl = null;
    
    private ?ResponseInterface $guzzleResponse = null;

    /**
     * @throws Throwable
     */
    public function execute(string $downloadUrl): GetStreamDTO
    {
        $this->downloadUrl = $downloadUrl;

        $client = new Client();
        $this->collectExecTime(
            fn () => $this->guzzleResponse = $client->get($this->downloadUrl, ['stream' => true])
        );

        return new GetStreamDTO(
            $this->getRequestLog(),
            $this->getResponseStatusCode(),
            $this->getResponseHeaders(),
            $this->guzzleResponse?->getBody(),
        );
    }

    protected function getRequestUrl(): string
    {
        return $this->downloadUrl;
    }

    protected function getRequestHeaders(): HeaderCollection
    {
        return new HeaderCollection();
    }

    protected function getRequestAsString(): ?string
    {
        return null;
    }

    protected function getResponseStatusCode(): ?int
    {
        return $this->guzzleResponse?->getStatusCode();
    }

    protected function getResponseAsString(): ?string
    {
        return null;
    }

    protected function getResponseHeaders(): HeaderCollection
    {
        return HeaderCollection::fromGuzzleArray($this->guzzleResponse?->getHeaders() ?? []);
    }
}