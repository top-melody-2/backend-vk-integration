<?php

declare(strict_types=1);

namespace backendVkIntegration\infrastructure\operations\vk;

use backendIntegrationCore\domain\collections\HeaderCollection;
use backendIntegrationCore\infrastructure\operations\AbstractApiOperation;
use backendVkIntegration\domain\Dictionary;
use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;
use stdClass;
use Throwable;

abstract class AbstractVkApiOperation extends AbstractApiOperation
{

    private ?ResponseInterface $guzzleResponse = null;

    private ?string $responseString = null;

    protected ?stdClass $responseObject = null;

    final protected function getRequestUrl(): string
    {
        return Dictionary::BASE_URL . $this->getMethodUrl();
    }

    abstract protected function getMethodUrl(): string;

    final protected function getRequestAsString(): ?string
    {
        return $this->getRequest() ? json_encode($this->getRequest(), JSON_THROW_ON_ERROR) : null;
    }

    abstract protected function getRequest(): mixed;

    /**
     * @throws Throwable
     */
    final protected function sendRequest(): void
    {
        $client = new Client([
            'base_uri' => Dictionary::BASE_URL,
            'timeout' => Dictionary::REST_TIMEOUT_IN_SECONDS,
            'headers' => $this->getRequestHeaders()->toArray(),
        ]);

        $this->collectExecTime(
            fn () => $this->guzzleResponse = $client->post(
                $this->getMethodUrl(),
                ['form_params' => $this->getRequest()]
            )
        );

        $this->responseString = $this->guzzleResponse?->getBody()?->getContents();
        $this->responseObject = $this->responseString
            ? json_decode($this->responseString, flags: JSON_THROW_ON_ERROR)
            : null;
    }

    final protected function getResponseStatusCode(): ?int
    {
        return $this->guzzleResponse?->getStatusCode();
    }

    final protected function getResponseAsString(): ?string
    {
        return $this->responseString;
    }

    final protected function getResponseHeaders(): HeaderCollection
    {
        return HeaderCollection::fromGuzzleArray($this->guzzleResponse?->getHeaders() ?? []);
    }
}