<?php

declare(strict_types=1);

namespace backendVkIntegration\infrastructure\operations\vk;

use backendIntegrationCore\domain\collections\HeaderCollection;
use backendVkIntegration\domain\dto\operations\AudioAddDTO;
use backendVkIntegration\domain\dto\requests\AudioAddRequestDTO;
use backendVkIntegration\domain\dto\responses\AudioIdResponse;
use Throwable;

final class AudioAddOperation extends AbstractVkApiOperation
{

    private ?AudioAddRequestDTO $request = null;

    private ?HeaderCollection $requestHeaders = null;

    /**
     * @throws Throwable
     */
    public function execute(AudioAddRequestDTO $request, HeaderCollection $requestHeaders): AudioAddDTO
    {
        $this->request = $request;
        $this->requestHeaders = $requestHeaders;
        $this->sendRequest();

        return new AudioAddDTO(
            $this->getRequestLog(),
            $this->getResponseStatusCode(),
            $this->getResponseHeaders(),
            $this->responseObject ? new AudioIdResponse($this->responseObject) : null
        );
    }

    protected function getRequestHeaders(): HeaderCollection
    {
        return $this->requestHeaders;
    }

    protected function getMethodUrl(): string
    {
        return '/method/audio.add/';
    }

    protected function getRequest(): mixed
    {
        return $this->request;
    }

    protected function getResponse(): ?AudioIdResponse
    {
        return $this->responseObject ? new AudioIdResponse($this->responseObject) : null;
    }
}