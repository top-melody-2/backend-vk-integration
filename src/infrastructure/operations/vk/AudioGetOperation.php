<?php

declare(strict_types=1);

namespace backendVkIntegration\infrastructure\operations\vk;

use backendIntegrationCore\domain\collections\HeaderCollection;
use backendVkIntegration\domain\dto\AudioListDTO;
use backendVkIntegration\domain\dto\operations\AudioGetDTO;
use backendVkIntegration\domain\dto\requests\AudioGetRequestDTO;
use backendVkIntegration\domain\dto\responses\AudioListResponse;
use Throwable;

final class AudioGetOperation extends AbstractVkApiOperation
{

    private ?AudioGetRequestDTO $request = null;

    private ?HeaderCollection $requestHeaders = null;

    /**
     * @throws Throwable
     */
    public function execute(AudioGetRequestDTO $request, HeaderCollection $requestHeaders): AudioGetDTO
    {
        $this->request = $request;
        $this->requestHeaders = $requestHeaders;
        $this->sendRequest();

        return new AudioGetDTO(
            $this->getRequestLog(),
            $this->getResponseStatusCode(),
            $this->getResponseHeaders(),
            $this->responseObject ? new AudioListResponse($this->responseObject) : null
        );
    }

    protected function getRequestHeaders(): HeaderCollection
    {
        return $this->requestHeaders;
    }

    protected function getMethodUrl(): string
    {
        return '/method/audio.get/';
    }

    protected function getRequest(): mixed
    {
        return $this->request;
    }
}