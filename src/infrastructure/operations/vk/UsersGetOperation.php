<?php

declare(strict_types=1);

namespace backendVkIntegration\infrastructure\operations\vk;

use backendIntegrationCore\domain\collections\HeaderCollection;
use backendVkIntegration\domain\dto\operations\UsersGetDTO;
use backendVkIntegration\domain\dto\requests\UsersGetRequestDTO;
use backendVkIntegration\domain\dto\responses\UserListResponse;
use Throwable;

final class UsersGetOperation extends AbstractVkApiOperation
{

    private ?UsersGetRequestDTO $request = null;

    private ?HeaderCollection $requestHeaders = null;

    /**
     * @throws Throwable
     */
    public function execute(UsersGetRequestDTO $request, HeaderCollection $requestHeaders): UsersGetDTO
    {
        $this->request = $request;
        $this->requestHeaders = $requestHeaders;
        $this->sendRequest();

        return new UsersGetDTO(
            $this->getRequestLog(),
            $this->getResponseStatusCode(),
            $this->getResponseHeaders(),
            $this->responseObject ? new UserListResponse($this->responseObject) : null
        );
    }

    protected function getRequestHeaders(): HeaderCollection
    {
        return $this->requestHeaders;
    }

    protected function getMethodUrl(): string
    {
        return '/method/users.get/';
    }

    protected function getRequest(): mixed
    {
        return $this->request;
    }
}