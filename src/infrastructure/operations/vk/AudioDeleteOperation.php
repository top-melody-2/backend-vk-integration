<?php

declare(strict_types=1);

namespace backendVkIntegration\infrastructure\operations\vk;

use backendIntegrationCore\domain\collections\HeaderCollection;
use backendVkIntegration\domain\dto\operations\AudioDeleteDTO;
use backendVkIntegration\domain\dto\requests\AudioDeleteRequestDTO;
use backendVkIntegration\domain\dto\responses\BooleanResponse;
use Throwable;

final class AudioDeleteOperation extends AbstractVkApiOperation
{

    private ?AudioDeleteRequestDTO $request = null;

    private ?HeaderCollection $requestHeaders = null;

    /**
     * @throws Throwable
     */
    public function execute(AudioDeleteRequestDTO $request, HeaderCollection $requestHeaders): AudioDeleteDTO
    {
        $this->request = $request;
        $this->requestHeaders = $requestHeaders;
        $this->sendRequest();

        return new AudioDeleteDTO(
            $this->getRequestLog(),
            $this->getResponseStatusCode(),
            $this->getResponseHeaders(),
            $this->responseObject ? new BooleanResponse($this->responseObject) : null
        );
    }

    protected function getRequestHeaders(): HeaderCollection
    {
        return $this->requestHeaders;
    }

    protected function getMethodUrl(): string
    {
        return '/method/audio.delete/';
    }

    protected function getRequest(): mixed
    {
        return $this->request;
    }
}