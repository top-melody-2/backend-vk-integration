<?php

declare(strict_types=1);

namespace backendVkIntegration\infrastructure\operations;

use backendIntegrationCore\domain\collections\HeaderCollection;
use backendIntegrationCore\infrastructure\operations\AbstractApiOperation;
use backendVkIntegration\domain\dto\operations\AuthDTO;
use backendVkIntegration\domain\dto\requests\AuthRequestDTO;
use backendVkIntegration\domain\dto\responses\AuthResponse;
use Throwable;
use Vodka2\VKAudioToken\TokenFacade;

final class AuthOperation extends AbstractApiOperation
{

    private ?AuthRequestDTO $request = null;
    private ?array $rawResponse = null;

    /**
     * @throws Throwable
     */
    public function execute(AuthRequestDTO $requestDTO): AuthDTO
    {
        $this->request = $requestDTO;

        $this->collectExecTime(
            fn () => $this->rawResponse = TokenFacade::getKateToken($this->request->login, $this->request->password)
        );

        return new AuthDTO(
            $this->getRequestLog(),
            $this->getResponseStatusCode(),
            $this->getResponseHeaders(),
            new AuthResponse($this->rawResponse),
        );
    }

    protected function getRequestUrl(): string
    {
        return '';
    }

    protected function getRequestHeaders(): HeaderCollection
    {
        return new HeaderCollection();
    }

    protected function getRequestAsString(): ?string
    {
        return null;
    }

    protected function getResponseStatusCode(): ?int
    {
        return null;
    }

    protected function getResponseAsString(): ?string
    {
        return $this->rawResponse ? json_encode($this->rawResponse, JSON_THROW_ON_ERROR) : null;
    }

    protected function getResponseHeaders(): HeaderCollection
    {
        return new HeaderCollection();
    }
}