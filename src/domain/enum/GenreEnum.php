<?php

declare(strict_types=1);

namespace backendVkIntegration\domain\enum;

enum GenreEnum: int
{
    case Rock = 1;
    case Pop = 2;
    case RapAndHipHop = 3;
    case EasyListening = 4;
    case DanceAndHouse = 5;
    case Instrumental = 6;
    case Metal = 7;
    case Dubstep = 8;
    case JazzAndBlues = 9;
    case DrumAndBass = 10;
    case Trance = 11;
    case Chanson = 12;
    case Ethnic = 13;
    case AcousticAndVocal = 14;
    case Reggae = 15;
    case Classical = 16;
    case IndiePop = 17;
    case Other = 18;
    case Speech = 19;
    case Alternative = 21;
    case ElectroPopAndDisco = 22;
    case Undefined1 = 250;
    case Undefined2 = 1001;

    public function getDescription(): string
    {
        return match ($this) {
            self::Rock => 'Rock',
            self::Pop => 'Pop',
            self::RapAndHipHop => 'Rap & Hip-Hop',
            self::EasyListening => 'Easy Listening',
            self::DanceAndHouse => 'Dance & House',
            self::Instrumental => 'Instrumental',
            self::Metal => 'Metal',
            self::Dubstep => 'Dubstep',
            self::JazzAndBlues => 'Jazz & Blues',
            self::DrumAndBass => 'Drum & Bass',
            self::Trance => 'Trance',
            self::Chanson => 'Chanson',
            self::Ethnic => 'Ethnic',
            self::AcousticAndVocal => 'Acoustic & Vocal',
            self::Reggae => 'Reggae',
            self::Classical => 'Classical',
            self::IndiePop => 'Indie Pop',
            self::Other => 'Other',
            self::Speech => 'Speech',
            self::Alternative => 'Alternative',
            self::ElectroPopAndDisco => 'Electropop & Disco',
            self::Undefined1 => 'Undefined 1',
            self::Undefined2 => 'Undefined 2',
        };
    }
}
