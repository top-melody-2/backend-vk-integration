<?php

declare(strict_types=1);

namespace backendVkIntegration\domain\dto;

use stdClass;

final class ErrorDTO
{

    public function __construct(readonly private stdClass $error) {}

    public function getErrorCode(): ?int
    {
        return $this->error->error_code;
    }

    public function getErrorMsg(): ?string
    {
        return $this->error->error_msg;
    }

    /**
     * @return RequestParamDTO[]
     */
    public function getRequestParams(): array
    {
        return array_map(
            static fn (stdClass $param): RequestParamDTO => new RequestParamDTO($param),
            $this->error->request_params ?? []
        );
    }
}