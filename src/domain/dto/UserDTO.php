<?php

declare(strict_types=1);

namespace backendVkIntegration\domain\dto;

use stdClass;

final class UserDTO
{

    public function __construct(readonly private stdClass $user) {}

    public function getId(): ?int
    {
        return $this->user->id;
    }

    /**
     * Имя
     */
    public function getFirstName(): ?string
    {
        return $this->user->first_name;
    }

    /**
     * Фамилия
     */
    public function getLastName(): ?string
    {
        return $this->user->last_name;
    }

    /**
     * Есть ли у текущего пользователя возможность видеть профиль пользователя при is_closed == true?
     */
    public function getCanAccessClosed(): ?bool
    {
        return $this->user->can_access_colsed;
    }

    /**
     * Включена ли приватность профиля?
     */
    public function getIsClosed(): ?bool
    {
        return $this->user->is_closed;
    }
}