<?php

declare(strict_types=1);

namespace backendVkIntegration\domain\dto\requests;

use backendVkIntegration\domain\Dictionary;

class AudioGetRequestDTO extends AbstractRequestDTO
{
    /**
     * @param int[]|null $audio_ids
     */
    public function __construct(
        public string $access_token,
        public int $offset = 0,
        public int $count = Dictionary::AUDIO_GET_MAX_COUNT,
        public ?array $audio_ids = null,
    ) {}
}