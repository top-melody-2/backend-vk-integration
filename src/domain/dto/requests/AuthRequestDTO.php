<?php

declare(strict_types=1);

namespace backendVkIntegration\domain\dto\requests;

class AuthRequestDTO
{
    public function __construct(
        public string $login,
        public string $password,
    ) {}
}