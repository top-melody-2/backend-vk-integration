<?php

declare(strict_types=1);

namespace backendVkIntegration\domain\dto\requests;

use backendVkIntegration\domain\Dictionary;

class AbstractRequestDTO
{
    public float $v = Dictionary::API_VERSION;
}