<?php

declare(strict_types=1);

namespace backendVkIntegration\domain\dto\requests;

class UsersGetRequestDTO extends AbstractRequestDTO
{
    public function __construct(
        public string $access_token,
        public int $count = 1,
    ) {}
}