<?php

declare(strict_types=1);

namespace backendVkIntegration\domain\dto\requests;

class AudioAddRequestDTO extends AbstractRequestDTO
{
    public function __construct(
        public string $access_token,
        public int $audio_id,
        public ?int $owner_id = null,
        public ?int $group_id = null,
    ) {}
}