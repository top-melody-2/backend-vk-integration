<?php

declare(strict_types=1);

namespace backendVkIntegration\domain\dto\operations;

use backendIntegrationCore\domain\collections\HeaderCollection;
use backendIntegrationCore\domain\dto\AbstractOperationDTO;
use backendIntegrationCore\domain\dto\RequestLogDTO;
use backendVkIntegration\domain\dto\responses\AudioListResponse;

class AudioGetDTO extends AbstractOperationDTO
{
    public function __construct(
        public RequestLogDTO $requestLog,
        public ?int $responseStatusCode,
        public HeaderCollection $responseHeaders,
        public ?AudioListResponse $response,
    ) {}
}