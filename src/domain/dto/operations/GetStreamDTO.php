<?php

declare(strict_types=1);

namespace backendVkIntegration\domain\dto\operations;

use backendIntegrationCore\domain\collections\HeaderCollection;
use backendIntegrationCore\domain\dto\AbstractOperationDTO;
use backendIntegrationCore\domain\dto\RequestLogDTO;
use Psr\Http\Message\StreamInterface;

class GetStreamDTO extends AbstractOperationDTO
{

    public function __construct(
        public RequestLogDTO $requestLog,
        public ?int $responseStatusCode,
        public HeaderCollection $responseHeaders,
        public ?StreamInterface $response
    ) {}
}