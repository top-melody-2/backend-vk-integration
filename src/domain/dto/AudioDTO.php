<?php

declare(strict_types=1);

namespace backendVkIntegration\domain\dto;

use backendVkIntegration\domain\enum\GenreEnum;
use stdClass;
use DateTimeImmutable;
use DateTimeInterface;

final class AudioDTO
{

    public function __construct(readonly private stdClass $audio) {}

    /**
     * Автор
     */
    public function getArtist(): ?string
    {
        return $this->audio->artist;
    }

    public function getId(): ?int
    {
        return $this->audio->id;
    }

    /**
     * ID владельца (у кого добавлена запись)
     */
    public function getOwnerId(): ?int
    {
        return $this->audio->owner_id;
    }

    public function getTitle(): ?string
    {
        return $this->audio->title;
    }

    /**
     * Длительность в секундах
     */
    public function getDuration(): ?int
    {
        return $this->audio->duration;
    }

    public function getAccessKey(): ?string
    {
        return $this->audio->access_key;
    }

    public function getAds(): ?AdsDTO
    {
        $ads = $this->audio->ads;
        return $ads ? new AdsDTO($ads) : null;
    }

    public function getIsExplicit(): ?bool
    {
        return $this->audio->is_explicit;
    }

    public function getIsLicensed(): ?bool
    {
        return $this->audio->is_licensed;
    }

    public function getTrackCode(): ?string
    {
        return $this->audio->track_code;
    }

    /**
     * Ссылка на скачивание
     */
    public function getUrl(): ?string
    {
        return $this->audio->url;
    }

    /**
     * Дата добавления в аудиозаписи
     */
    public function getDate(): ?DateTimeInterface
    {
        $unixTimestamp = $this->audio->date;
        $maybeDateTime = $unixTimestamp ? DateTimeImmutable::createFromFormat('U', $unixTimestamp) : null;
        return $maybeDateTime === false ? null : $maybeDateTime;
    }

    /**
     * Не предлагать в поиске
     */
    public function getNoSearch(): ?bool
    {
        return $this->audio->no_search;
    }

    /**
     * ID жанра
     */
    public function getGenreId(): ?GenreEnum
    {
        $intValue = $this->audio->genre_id;
        return $intValue !== null ? GenreEnum::tryFrom($intValue) : null;
    }

    /**
     * Ограниченный контент
     */
    public function getContentRestricted(): ?int
    {
        return $this->audio->content_restricted;
    }

    /**
     * Артисты
     * @return ArtistDTO[]
     */
    public function getMainArtists(): array
    {
        return array_map(
            static fn (stdClass $artist): ArtistDTO => new ArtistDTO($artist),
            $this->audio->main_artists ?? []
        );
    }

    /**
     * Избранные артисты
     * @return ArtistDTO[]
     */
    public function getFeaturedArtists(): array
    {
        return array_map(
            static fn (stdClass $artist): ArtistDTO => new ArtistDTO($artist),
            $this->audio->featured_artists ?? []
        );
    }

    /**
     * Описание
     */
    public function getSubtitle(): ?string
    {
        return $this->audio->subtitle;
    }

    public function getShortVideosAllowed(): ?bool
    {
        return $this->audio->short_videos_allowed;
    }

    public function getStoriesAllowed(): ?bool
    {
        return $this->audio->stories_allowed;
    }

    public function getStoriesCoverAllowed(): ?bool
    {
        return $this->audio->stories_cover_allowed;
    }
}