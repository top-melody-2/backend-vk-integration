<?php

declare(strict_types=1);

namespace backendVkIntegration\domain\dto;

use stdClass;

final class ArtistDTO
{

    public function __construct(readonly private stdClass $artist) {}

    public function getId(): ?int
    {
        return $this->artist->id;
    }

    public function getName(): ?string
    {
        return $this->artist->name;
    }

    public function getDomain(): ?int
    {
        return $this->artist->domain;
    }
}