<?php

declare(strict_types=1);

namespace backendVkIntegration\domain\dto;

readonly class NextAudioDTO
{

    public function __construct(
        public AudioDTO $audioDTO,
        public int $offset,
    ) {}
}