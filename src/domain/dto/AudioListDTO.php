<?php

declare(strict_types=1);

namespace backendVkIntegration\domain\dto;

use stdClass;

final class AudioListDTO
{

    public function __construct(readonly private stdClass $audioList) {}

    public function getCount(): ?int
    {
        return $this->audioList->count;
    }

    /**
     * @return AudioDTO[]
     */
    public function getItems(): array
    {
        return array_map(
            static fn (stdClass $item): AudioDTO => new AudioDTO($item),
            $this->audioList->items ?? []
        );
    }
}