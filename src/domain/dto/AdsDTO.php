<?php

declare(strict_types=1);

namespace backendVkIntegration\domain\dto;

use stdClass;

final class AdsDTO
{

    public function __construct(readonly private stdClass $audioAds) {}

    public function getContentId(): ?string
    {
        return $this->audioAds->content_id;
    }

    public function getDuration(): ?int
    {
        $string = $this->audioAds->duration;
        return $string ? intval($string) : null;
    }

    public function getAccountAgeType(): ?int
    {
        $string = $this->audioAds->account_age_type;
        return $string ? intval($string) : null;
    }

    public function getPuid1(): ?int
    {
        $string = $this->audioAds->puid1;
        return $string ? intval($string) : null;
    }

    public function getPuid22(): ?int
    {
        $string = $this->audioAds->puid22;
        return $string ? intval($string) : null;
    }
}