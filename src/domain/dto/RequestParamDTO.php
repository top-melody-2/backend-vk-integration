<?php

declare(strict_types=1);

namespace backendVkIntegration\domain\dto;

use stdClass;

final class RequestParamDTO
{

    public function __construct(readonly private stdClass $requestParam) {}

    public function getKey(): ?string
    {
        return $this->requestParam->key;
    }

    public function getValue(): ?string
    {
        return $this->requestParam->value;
    }
}