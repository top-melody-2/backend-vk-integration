<?php

declare(strict_types=1);

namespace backendVkIntegration\domain\dto\responses;

class AudioIdResponse extends AbstractResponse
{

    public function getResponse(): ?int
    {
        return $this->response->response;
    }
}