<?php

declare(strict_types=1);

namespace backendVkIntegration\domain\dto\responses;

class BooleanResponse extends AbstractResponse
{

    public function getResponse(): bool
    {
        return $this->response->response === 1;
    }
}