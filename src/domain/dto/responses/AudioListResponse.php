<?php

declare(strict_types=1);

namespace backendVkIntegration\domain\dto\responses;

use backendVkIntegration\domain\dto\AudioListDTO;
use stdClass;

class AudioListResponse extends AbstractResponse
{

    public function getResponse(): ?AudioListDTO
    {
        $object = $this->response->response;
        return $object ? new AudioListDTO($object) : null;
    }
}