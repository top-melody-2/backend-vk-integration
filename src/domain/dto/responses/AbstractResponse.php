<?php

declare(strict_types=1);

namespace backendVkIntegration\domain\dto\responses;

use backendVkIntegration\domain\dto\ErrorDTO;
use stdClass;

abstract class AbstractResponse
{

    public function __construct(readonly protected stdClass $response) {}

    public function getError(): ?ErrorDTO
    {
        $object = $this->response->error ?? null;
        return $object ? new ErrorDTO($object) : null;
    }
}