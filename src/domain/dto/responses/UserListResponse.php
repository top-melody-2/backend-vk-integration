<?php

declare(strict_types=1);

namespace backendVkIntegration\domain\dto\responses;

use backendVkIntegration\domain\dto\UserDTO;
use stdClass;

class UserListResponse extends AbstractResponse
{

    /**
     * @return UserDTO[]
     */
    public function getResponse(): array
    {
        return array_map(
            static fn (stdClass $user): UserDTO => new UserDTO($user),
            $this->response->response ?? []
        );
    }
}