<?php

declare(strict_types=1);

namespace backendVkIntegration\domain\dto\responses;

use stdClass;

final class AuthResponse
{
    public function __construct(readonly protected array $response) {}

    public function getToken(): ?string
    {
        return $this->response['token'];
    }

    public function getUserAgent(): ?string
    {
        return $this->response['userAgent'];
    }
}