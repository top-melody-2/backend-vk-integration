<?php

declare(strict_types=1);

namespace backendVkIntegration\domain\dto;

use backendIntegrationCore\domain\dto\StreamInterface;
use backendIntegrationCore\domain\dto\TrackDTO;
use backendVkIntegration\domain\Dictionary;

class StreamDTO implements StreamInterface
{

    public function __construct(
        private \Psr\Http\Message\StreamInterface $stream,
        private string $trackName,
        private int $sizeInBytes,
    ) {}

    public function getStream(): callable
    {
        return function() {
            while (!$this->stream->eof()) {
                yield $this->stream->read(Dictionary::STREAM_BATCH_SIZE);
            }

            $this->stream->close();
        };
    }

    public function getName(): string
    {
        return $this->trackName;
    }

    public function getExtension(): string
    {
        return 'mp3';
    }

    public function getSizeInBytes(): int
    {
        return $this->stream->getSize() ?? $this->sizeInBytes;
    }
}