<?php

declare(strict_types=1);

namespace backendVkIntegration\domain;

use backendIntegrationCore\domain\collections\ConfigCollection;
use backendIntegrationCore\domain\dto\IntegrationDTO;
use backendIntegrationCore\domain\dto\PlaylistDTO;
use backendIntegrationCore\domain\dto\TrackDTO;
use backendIntegrationCore\domain\dto\responses\ChangeTrackStateResponseDTO;
use backendIntegrationCore\domain\dto\responses\GetInfoResponseDTO;
use backendIntegrationCore\domain\dto\responses\GetPlaylistsResponseDTO;
use backendIntegrationCore\domain\dto\responses\GetTrackResponseDTO;
use backendIntegrationCore\domain\dto\responses\GetTrackStreamResponseDTO;
use backendIntegrationCore\domain\IntegrationInterface;
use backendVkIntegration\domain\services\GetTrackStreamService;
use backendVkIntegration\domain\services\vk\DislikeTrackVkService;
use backendVkIntegration\domain\services\vk\GetNextTrackVkService;
use backendVkIntegration\domain\services\vk\GetPlaylistsVkService;
use backendVkIntegration\domain\services\vk\LikeTrackVkService;

class VkIntegration implements IntegrationInterface
{

    public function __construct(
        private GetPlaylistsVkService $getPlaylistsVkService,
        private GetNextTrackVkService $getNextTrackVkService,
        private GetTrackStreamService $getTrackStreamService,
        private LikeTrackVkService $likeTrackVkService,
        private DislikeTrackVkService $dislikeTrackVkService,
    ) {}

    /**
     * @inheritDoc
     */
    public function getInfo(): GetInfoResponseDTO
    {
        return new GetInfoResponseDTO(
            new IntegrationDTO(Dictionary::INTEGRATION_NAME, new ConfigCollection())
        );
    }

    /**
     * @inheritDoc
     */
    public function getPlaylists(IntegrationDTO $integrationDTO): GetPlaylistsResponseDTO
    {
        return $this->getPlaylistsVkService->service($integrationDTO);
    }

    /**
     * @inheritDoc
     */
    public function getNextTrack(IntegrationDTO $integrationDTO, PlaylistDTO $playlistDTO): GetTrackResponseDTO
    {
        return $this->getNextTrackVkService->service($integrationDTO, $playlistDTO);
    }

    /**
     * @inheritDoc
     */
    public function getTrackStream(IntegrationDTO $integrationDTO, TrackDTO $trackDTO): GetTrackStreamResponseDTO
    {
        return $this->getTrackStreamService->service($trackDTO);
    }

    /**
     * @inheritDoc
     */
    public function canLikeTrack(IntegrationDTO $integrationDTO, PlaylistDTO $playlistDTO, TrackDTO $trackDTO): bool
    {
        return $playlistDTO->name !== Dictionary::PLAYLIST_MY_MUSIC_NAME;
    }

    /**
     * @inheritDoc
     */
    public function likeTrack(IntegrationDTO $integrationDTO, PlaylistDTO $playlistDTO, TrackDTO $trackDTO): ChangeTrackStateResponseDTO
    {
        return $this->likeTrackVkService->service($integrationDTO, $playlistDTO, $trackDTO);
    }

    /**
     * @inheritDoc
     */
    public function canDislikeTrack(IntegrationDTO $integrationDTO, PlaylistDTO $playlistDTO, TrackDTO $trackDTO): bool
    {
        return $playlistDTO->name === Dictionary::PLAYLIST_MY_MUSIC_NAME;
    }

    /**
     * @inheritDoc
     */
    public function dislikeTrack(IntegrationDTO $integrationDTO, PlaylistDTO $playlistDTO, TrackDTO $trackDTO): ChangeTrackStateResponseDTO
    {
        return $this->dislikeTrackVkService->service($integrationDTO, $playlistDTO, $trackDTO);
    }
}