<?php

declare(strict_types=1);

namespace backendVkIntegration\domain\services;

use backendIntegrationCore\domain\dto\ApiOperationResultDTO;
use backendIntegrationCore\domain\dto\IntegrationDTO;
use backendIntegrationCore\domain\dto\RequestLogDTO;
use backendIntegrationCore\domain\dto\responses\GetTrackStreamResponseDTO;
use backendIntegrationCore\domain\dto\StreamInterface;
use backendIntegrationCore\domain\dto\TrackDTO;
use backendIntegrationCore\domain\exceptions\IntegrationException;
use backendIntegrationCore\domain\exceptions\NeedLogException;
use backendVkIntegration\domain\Dictionary;
use backendVkIntegration\domain\dto\operations\GetStreamDTO;
use backendVkIntegration\domain\dto\StreamDTO;
use backendVkIntegration\infrastructure\operations\GetStreamOperation;
use Throwable;

class GetTrackStreamService
{

    public function __construct(private GetStreamOperation $getStreamOperation) {}

    /**
     * @throws IntegrationException
     * @throws NeedLogException
     */
    public function service(TrackDTO $trackDTO): GetTrackStreamResponseDTO
    {
        $downloadUrl = $trackDTO->config->get(Dictionary::TRACK_CONFIG_KEY_DOWNLOAD_URL);
        try {
            $getStreamDTO = $this->getStreamOperation->execute($downloadUrl);
        } catch (Throwable $exception) {
            throw new IntegrationException($exception->getMessage());
        }
        
        $this->validateResponse($getStreamDTO);

        return new GetTrackStreamResponseDTO(
            [$getStreamDTO->requestLog],
            $this->getStream($getStreamDTO, $trackDTO)
        );
    }

    /**
     * @throws NeedLogException
     */
    private function validateResponse(GetStreamDTO $getStreamDTO): void
    {
        if (!$getStreamDTO->response) {
            throw new NeedLogException(
                Dictionary::ERROR_MESSAGE_NO_RESPONSE_RECEIVED,
                [$getStreamDTO->requestLog]
            );
        }

        if (!in_array($getStreamDTO->responseStatusCode, Dictionary::SUCCESS_STATUS_CODES)) {
            $message = sprintf(
                Dictionary::ERROR_MESSAGE_TEMPLATE_INVALID_RESPONSE_STATUS_CODE,
                $getStreamDTO->responseStatusCode
            );
            throw new NeedLogException($message, [$getStreamDTO->requestLog]);
        }
    }

    public function getStream(GetStreamDTO $getStreamDTO, TrackDTO $trackDTO): StreamInterface
    {
        return new StreamDTO(
            $getStreamDTO->response,
            $trackDTO->name,
            (int) $getStreamDTO->responseHeaders->get('Content-Length', 0),
        );
    }
}