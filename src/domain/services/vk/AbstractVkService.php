<?php

declare(strict_types=1);

namespace backendVkIntegration\domain\services\vk;

use backendIntegrationCore\domain\collections\ConfigCollection;
use backendIntegrationCore\domain\collections\HeaderCollection;
use backendIntegrationCore\domain\dto\AbstractOperationDTO;
use backendIntegrationCore\domain\dto\IntegrationDTO;
use backendIntegrationCore\domain\dto\RequestLogDTO;
use backendIntegrationCore\domain\exceptions\NeedLogException;
use backendVkIntegration\domain\Dictionary;
use backendVkIntegration\domain\dto\operations\AuthDTO;
use backendVkIntegration\domain\dto\operations\UsersGetDTO;
use backendVkIntegration\domain\dto\requests\AuthRequestDTO;
use backendVkIntegration\domain\dto\requests\UsersGetRequestDTO;
use backendVkIntegration\domain\dto\responses\AbstractResponse;
use backendVkIntegration\domain\dto\responses\AuthResponse;
use backendVkIntegration\domain\dto\responses\UserListResponse;
use backendVkIntegration\infrastructure\operations\AuthOperation;
use backendVkIntegration\infrastructure\operations\vk\UsersGetOperation;
use Throwable;

abstract class AbstractVkService
{

    /**
     * @var RequestLogDTO[]
     */
    private array $requestLogs = [];

    private ?AuthResponse $authResponseCache = null;

    private ?int $userIdCache = null;

    public function __construct(
        private AuthOperation $authOperation,
        private UsersGetOperation $usersGetOperation,
    ) {}

    /**
     * @throws NeedLogException
     */
    final protected function executeOperation(callable $func): AbstractOperationDTO
    {
        /** @var AbstractOperationDTO $operationDTO */
        $operationDTO = $this->tryOperation($func);

        /** @var AbstractResponse $response */
        $response = $operationDTO->response;
        if (!$response) {
            throw new NeedLogException(Dictionary::ERROR_MESSAGE_NO_RESPONSE_RECEIVED, $this->requestLogs);
        }

        if (!in_array($operationDTO->responseStatusCode, Dictionary::SUCCESS_STATUS_CODES)) {
            $message = sprintf(
                Dictionary::ERROR_MESSAGE_TEMPLATE_INVALID_RESPONSE_STATUS_CODE,
                $operationDTO->responseStatusCode
            );
            throw new NeedLogException($message, $this->requestLogs);
        }

        if ($response->getError()) {
            switch ($response->getError()->getErrorCode()) {
                case Dictionary::ERROR_CODE_TOKEN_IS_EXPIRED:
                    $this->authResponseCache = $this->getAuthResponse();
                    $operationDTO = $this->tryOperation($func);
                    $this->requestLogs[] = $operationDTO->requestLog;
                    break;
                default:
                    $message = $response->getError()->getErrorMsg() ?? Dictionary::ERROR_MESSAGE_NO_ERROR_MESSAGE;
                    throw new NeedLogException($message, $this->requestLogs);
            }
        }

        return $operationDTO;
    }

    /**
     * @throws NeedLogException
     */
    private function tryOperation(callable $func): AbstractOperationDTO
    {
        try {
            /** @var AbstractOperationDTO $operationResult */
            $operationResult = $func();
            $this->requestLogs[] = $operationResult->requestLog;
        } catch (Throwable $exception) {
            throw new NeedLogException($exception->getMessage(), $this->requestLogs);
        }

        return $operationResult;
    }

    final protected function getRequestLogs(): array
    {
        return $this->requestLogs;
    }

    /**
     * @throws NeedLogException
     */
    final protected function getToken(IntegrationDTO $integrationDTO): string
    {
        if ($this->authResponseCache) {
            return $this->authResponseCache->getToken();
        }

        if ($integrationDTO->config->has(Dictionary::INTEGRATION_CONFIG_KEY_TOKEN)) {
            return $integrationDTO->config->get(Dictionary::INTEGRATION_CONFIG_KEY_TOKEN);
        }

        $this->authResponseCache = $this->getAuthResponse();
        return $this->authResponseCache->getToken();
    }

    /**
     * @throws NeedLogException
     */
    final protected function getUserAgent(IntegrationDTO $integrationDTO): string
    {
        if ($this->authResponseCache) {
            return $this->authResponseCache->getUserAgent();
        }

        if ($integrationDTO->config->has(Dictionary::INTEGRATION_CONFIG_KEY_USER_AGENT)) {
            return $integrationDTO->config->get(Dictionary::INTEGRATION_CONFIG_KEY_USER_AGENT);
        }

        $this->authResponseCache = $this->getAuthResponse();
        return $this->authResponseCache->getUserAgent();
    }

    /**
     * @throws NeedLogException
     */
    private function getAuthResponse(): AuthResponse
    {
        $request = new AuthRequestDTO(Dictionary::LOGIN, Dictionary::PASSWORD);
        /** @var AuthDTO $authDTO */
        $authDTO = $this->tryOperation(fn () => $this->authOperation->execute($request));

        if (!$authDTO->response->getToken()) {
            throw new NeedLogException(Dictionary::ERROR_MESSAGE_NOT_FOUND_TOKEN_IN_AUTH_RESPONSE, $this->requestLogs);
        }
        if (!$authDTO->response->getUserAgent()) {
            throw new NeedLogException(Dictionary::ERROR_MESSAGE_NOT_FOUND_USER_AGENT_IN_AUTH_RESPONSE, $this->requestLogs);
        }

        return $authDTO->response;
    }

    /**
     * @throws NeedLogException
     */
    final protected function getUserId(IntegrationDTO $integrationDTO): int
    {
        if ($this->userIdCache) {
            return $this->userIdCache;
        }

        if ($integrationDTO->config->has(Dictionary::INTEGRATION_CONFIG_KEY_USER_ID)) {
            return $integrationDTO->config->get(Dictionary::INTEGRATION_CONFIG_KEY_USER_ID);
        }

        $request = new UsersGetRequestDTO($this->getToken($integrationDTO));
        $requestHeaders = new HeaderCollection(['User-Agent' => $this->getUserAgent($integrationDTO)]);
        /** @var UsersGetDTO $usersGetResult */
        $usersGetResult = $this->tryOperation(fn () => $this->usersGetOperation->execute($request, $requestHeaders));

        $userId = $usersGetResult->response->getResponse()[0]?->getId();
        if (!$userId) {
            throw new NeedLogException(Dictionary::ERROR_MESSAGE_USER_ID_NOT_RECEIVED, $this->requestLogs);
        }

        $this->userIdCache = $userId;
        return $userId;
    }

    final protected function getIntegrationConfig(IntegrationDTO $integrationDTO): ConfigCollection
    {
        $integrationConfig = $integrationDTO->config;

        if ($this->authResponseCache) {
            $integrationConfig->set(Dictionary::INTEGRATION_CONFIG_KEY_TOKEN, $this->authResponseCache->getToken());
            $integrationConfig->set(Dictionary::INTEGRATION_CONFIG_KEY_USER_AGENT, $this->authResponseCache->getUserAgent());
        }

        if ($this->userIdCache) {
            $integrationConfig->set(Dictionary::INTEGRATION_CONFIG_KEY_USER_ID, $this->userIdCache);
        }

        return $integrationConfig;
    }
}