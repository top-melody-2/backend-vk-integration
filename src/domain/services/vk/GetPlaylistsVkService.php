<?php

declare(strict_types=1);

namespace backendVkIntegration\domain\services\vk;

use backendIntegrationCore\domain\collections\ConfigCollection;
use backendIntegrationCore\domain\collections\HeaderCollection;
use backendIntegrationCore\domain\dto\ApiOperationResultDTO;
use backendIntegrationCore\domain\dto\IntegrationDTO;
use backendIntegrationCore\domain\dto\PlaylistDTO;
use backendIntegrationCore\domain\dto\responses\GetPlaylistsResponseDTO;
use backendIntegrationCore\domain\exceptions\NeedLogException;
use backendVkIntegration\domain\Dictionary;
use backendVkIntegration\domain\dto\requests\AudioGetRequestDTO;
use backendVkIntegration\domain\dto\responses\AudioListResponse;
use backendVkIntegration\infrastructure\operations\AuthOperation;
use backendVkIntegration\infrastructure\operations\vk\AudioGetOperation;
use backendVkIntegration\infrastructure\operations\vk\UsersGetOperation;
use Throwable;

class GetPlaylistsVkService extends AbstractVkService
{

    public function service(IntegrationDTO $integrationDTO): GetPlaylistsResponseDTO
    {
        $playlists[] = new PlaylistDTO(
            Dictionary::PLAYLIST_MY_MUSIC_NAME,
            new ConfigCollection(),
        );

        return new GetPlaylistsResponseDTO(
            $this->getRequestLogs(),
            $this->getIntegrationConfig($integrationDTO),
            $playlists,
        );
    }
}