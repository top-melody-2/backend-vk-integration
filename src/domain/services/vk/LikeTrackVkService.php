<?php

declare(strict_types=1);

namespace backendVkIntegration\domain\services\vk;

use backendIntegrationCore\domain\collections\ConfigCollection;
use backendIntegrationCore\domain\collections\HeaderCollection;
use backendIntegrationCore\domain\dto\IntegrationDTO;
use backendIntegrationCore\domain\dto\PlaylistDTO;
use backendIntegrationCore\domain\dto\responses\ChangeTrackStateResponseDTO;
use backendIntegrationCore\domain\dto\TrackDTO;
use backendIntegrationCore\domain\exceptions\NeedLogException;
use backendVkIntegration\domain\Dictionary;
use backendVkIntegration\domain\dto\operations\AudioAddDTO;
use backendVkIntegration\domain\dto\requests\AudioAddRequestDTO;
use backendVkIntegration\infrastructure\operations\AuthOperation;
use backendVkIntegration\infrastructure\operations\vk\AudioAddOperation;
use backendVkIntegration\infrastructure\operations\vk\UsersGetOperation;

class LikeTrackVkService extends AbstractVkService
{

    public function __construct(
        AuthOperation $authOperation,
        UsersGetOperation $usersGetOperation,
        private AudioAddOperation $audioAddOperation,
    ) {
        parent::__construct($authOperation, $usersGetOperation);
    }

    /**
     * @throws NeedLogException
     */
    public function service(
        IntegrationDTO $integrationDTO,
        PlaylistDTO $playlistDTO,
        TrackDTO $trackDTO
    ): ChangeTrackStateResponseDTO
    {
        $trackId = $trackDTO->config->get(Dictionary::TRACK_CONFIG_KEY_ID);
        $request = new AudioAddRequestDTO(
            $this->getToken($integrationDTO),
            $trackId,
            $this->getUserId($integrationDTO)
        );
        $requestHeaders = new HeaderCollection(['User-Agent' => $this->getUserAgent($integrationDTO)]);
        /** @var AudioAddDTO $audioAddDTO */
        $audioAddDTO = $this->executeOperation(fn() => $this->audioAddOperation->execute($request, $requestHeaders));

        if ($audioAddDTO->response->getResponse() !== $trackId) {
            $message = sprintf(Dictionary::ERROR_MESSAGE_TEMPLATE_LIKE_ERROR, $trackId);
            throw new NeedLogException($message, $this->getRequestLogs());
        }

        return new ChangeTrackStateResponseDTO(
            $this->getRequestLogs(),
            $this->getIntegrationConfig($integrationDTO),
            $playlistDTO->config,
            $trackDTO->config,
        );
    }
}