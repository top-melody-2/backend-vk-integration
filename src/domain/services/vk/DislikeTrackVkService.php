<?php

declare(strict_types=1);

namespace backendVkIntegration\domain\services\vk;

use backendIntegrationCore\domain\collections\ConfigCollection;
use backendIntegrationCore\domain\collections\HeaderCollection;
use backendIntegrationCore\domain\dto\IntegrationDTO;
use backendIntegrationCore\domain\dto\PlaylistDTO;
use backendIntegrationCore\domain\dto\responses\ChangeTrackStateResponseDTO;
use backendIntegrationCore\domain\dto\TrackDTO;
use backendIntegrationCore\domain\exceptions\NeedLogException;
use backendVkIntegration\domain\Dictionary;
use backendVkIntegration\domain\dto\operations\AudioDeleteDTO;
use backendVkIntegration\domain\dto\requests\AudioDeleteRequestDTO;
use backendVkIntegration\infrastructure\operations\AuthOperation;
use backendVkIntegration\infrastructure\operations\vk\AudioDeleteOperation;
use backendVkIntegration\infrastructure\operations\vk\UsersGetOperation;

class DislikeTrackVkService extends AbstractVkService
{

    public function __construct(
        AuthOperation $authOperation,
        UsersGetOperation $usersGetOperation,
        private AudioDeleteOperation $audioDeleteOperation,
    ) {
        parent::__construct($authOperation, $usersGetOperation);
    }

    /**
     * @throws NeedLogException
     */
    public function service(
        IntegrationDTO $integrationDTO,
        PlaylistDTO $playlistDTO,
        TrackDTO $trackDTO
    ): ChangeTrackStateResponseDTO
    {
        $trackId = $trackDTO->config->get(Dictionary::TRACK_CONFIG_KEY_ID);
        $request = new AudioDeleteRequestDTO(
            $this->getToken($integrationDTO),
            $trackId,
            $this->getUserId($integrationDTO),
        );
        $requestHeaders = new HeaderCollection(['User-Agent' => $this->getUserAgent($integrationDTO)]);
        /** @var AudioDeleteDTO $audioDeleteDTO */
        $audioDeleteDTO = $this->executeOperation(fn () => $this->audioDeleteOperation->execute($request, $requestHeaders));

        if (!$audioDeleteDTO->response->getResponse()) {
            $message = sprintf(Dictionary::ERROR_MESSAGE_TEMPLATE_DISLIKE_ERROR, $trackId);
            throw new NeedLogException($message, $this->getRequestLogs());
        }

        return new ChangeTrackStateResponseDTO(
            $this->getRequestLogs(),
            $this->getIntegrationConfig($integrationDTO),
            $this->getPlaylistConfig($playlistDTO, $trackId),
            $trackDTO->config,
        );
    }

    private function getPlaylistConfig(PlaylistDTO $playlistDTO): ConfigCollection
    {
        $playlistConfig = $playlistDTO->config;
        if ($playlistConfig->has(Dictionary::PLAYLIST_CONFIG_KEY_CURRENT_TRACK_OFFSET)) {
            $currentTrackOffset = $playlistConfig->get(Dictionary::PLAYLIST_CONFIG_KEY_CURRENT_TRACK_OFFSET);
            $newOffset = $currentTrackOffset > 0 ? $currentTrackOffset - 1 : 0;
            $playlistConfig->set(Dictionary::PLAYLIST_CONFIG_KEY_CURRENT_TRACK_OFFSET, $newOffset);
        }

        return $playlistConfig;
    }
}