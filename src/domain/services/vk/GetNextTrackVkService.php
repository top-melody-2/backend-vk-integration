<?php

declare(strict_types=1);

namespace backendVkIntegration\domain\services\vk;

use backendIntegrationCore\domain\collections\ConfigCollection;
use backendIntegrationCore\domain\collections\HeaderCollection;
use backendIntegrationCore\domain\dto\ApiOperationResultDTO;
use backendIntegrationCore\domain\dto\IntegrationDTO;
use backendIntegrationCore\domain\dto\PlaylistDTO;
use backendIntegrationCore\domain\dto\responses\GetTrackResponseDTO;
use backendIntegrationCore\domain\dto\TrackDTO;
use backendIntegrationCore\domain\exceptions\NeedLogException;
use backendVkIntegration\domain\Dictionary;
use backendVkIntegration\domain\dto\AudioDTO;
use backendVkIntegration\domain\dto\NextAudioDTO;
use backendVkIntegration\domain\dto\operations\AudioGetDTO;
use backendVkIntegration\domain\dto\requests\AudioGetRequestDTO;
use backendVkIntegration\domain\dto\responses\AudioListResponse;
use backendVkIntegration\infrastructure\operations\AuthOperation;
use backendVkIntegration\infrastructure\operations\vk\AudioGetOperation;
use backendVkIntegration\infrastructure\operations\vk\UsersGetOperation;

class GetNextTrackVkService extends AbstractVkService
{

    private const COUNT_OF_REQUEST_TRACKS = 3;
    private const MAX_COUNT_OF_REQUESTS = 3;

    public function __construct(
        AuthOperation $authOperation,
        UsersGetOperation $usersGetOperation,
        private AudioGetOperation $audioGetOperation,
    ) {
        parent::__construct($authOperation, $usersGetOperation);
    }

    public function service(IntegrationDTO $integrationDTO, PlaylistDTO $playlistDTO): GetTrackResponseDTO
    {
        $nextAudioDTO = $this->getNextAudio($integrationDTO, $playlistDTO);

        $trackId = $nextAudioDTO->audioDTO->getId();
        $trackName = implode(Dictionary::TRACK_AUTHOR_AND_NAME_SEPARATOR, [
            $nextAudioDTO->audioDTO->getArtist(),
            $nextAudioDTO->audioDTO->getTitle()
        ]);
        $trackDuration = $nextAudioDTO->audioDTO->getDuration();
        $trackDownloadUrl = $this->getMp3DownloadUrl($nextAudioDTO->audioDTO->getUrl());

        $playlistConfig = $playlistDTO->config;
        $playlistConfig->set(Dictionary::PLAYLIST_CONFIG_KEY_CURRENT_TRACK_OFFSET, $nextAudioDTO->offset);

        return new GetTrackResponseDTO(
            $this->getRequestLogs(),
            $this->getIntegrationConfig($integrationDTO),
            $playlistConfig,
            new TrackDTO(
                $trackName,
                $trackDuration,
                new ConfigCollection([
                    Dictionary::TRACK_CONFIG_KEY_ID => $trackId,
                    Dictionary::TRACK_CONFIG_KEY_DOWNLOAD_URL => $trackDownloadUrl
                ])
            )
        );
    }

    private function getNextAudio(IntegrationDTO $integrationDTO, PlaylistDTO $playlistDTO): NextAudioDTO
    {
        $nextAudioOffset = $playlistDTO->config->has(Dictionary::PLAYLIST_CONFIG_KEY_CURRENT_TRACK_OFFSET)
            ? $playlistDTO->config->get(Dictionary::PLAYLIST_CONFIG_KEY_CURRENT_TRACK_OFFSET) + 1
            : 0;
        $countOfRequests = 0;

        while (true) {
            if ($countOfRequests === self::MAX_COUNT_OF_REQUESTS) {
                $message = sprintf(
                    Dictionary::ERROR_MESSAGE_TEMPLATE_PLAYLIST_HAS_NO_AVAILABLE_TO_DOWNLOAD_TRACKS,
                    $playlistDTO->name
                );
                throw new NeedLogException($message, $this->getRequestLogs());
            }

            $request = new AudioGetRequestDTO(
                $this->getToken($integrationDTO),
                $nextAudioOffset,
                self::COUNT_OF_REQUEST_TRACKS
            );
            $requestHeaders = new HeaderCollection(['User-Agent' => $this->getUserAgent($integrationDTO)]);
            /** @var AudioGetDTO $audioGetDTO */
            $audioGetDTO = $this->executeOperation(fn () => $this->audioGetOperation->execute($request, $requestHeaders));
            $countOfRequests++;

            $audioDtoList = $audioGetDTO->response?->getResponse()?->getItems() ?? [];
            if (!$audioDtoList) {
                if ($countOfRequests > 1) {
                    $nextAudioOffset = 0; // Начинаем с начала плейлиста
                    continue;
                }

                $message = sprintf(Dictionary::ERROR_MESSAGE_TEMPLATE_PLAYLIST_HAS_NO_TRACKS, $playlistDTO->name);
                throw new NeedLogException($message, $this->getRequestLogs());
            }

            foreach ($audioDtoList as $audioDTO) {
                if ($this->isValidAudioDTO($audioDTO)) {
                    return new NextAudioDTO($audioDTO, $nextAudioOffset);
                }

                $nextAudioOffset++;
            }
        }
    }

    private function isValidAudioDTO(AudioDTO $audioDTO): bool
    {
        return $audioDTO->getId()
            && $audioDTO->getArtist()
            && $audioDTO->getTitle()
            && $audioDTO->getDuration()
            && $audioDTO->getUrl();
    }

    private function getMp3DownloadUrl(string $rawDownloadUrl): string
    {
        if (!strpos($rawDownloadUrl, "index.m3u8?")) {
            return $rawDownloadUrl;
        }

        if (str_contains($rawDownloadUrl, "/audios/")) {
            return preg_replace('~^(.+?)/[^/]+?/audios/([^/]+)/.+$~', '\\1/audios/\\2.mp3', $rawDownloadUrl);
        }

        return preg_replace('~^(.+?)/(p[0-9]+)/[^/]+?/([^/]+)/.+$~', '\\1/\\2/\\3.mp3', $rawDownloadUrl);
    }

}