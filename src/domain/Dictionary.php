<?php

declare(strict_types=1);

namespace backendVkIntegration\domain;

class Dictionary
{
    public const INTEGRATION_NAME = 'ВКонтакте';
    public const PLAYLIST_MY_MUSIC_NAME = 'Моя музыка';

    public const BASE_URL = 'https://api.vk.com';
    public const REST_TIMEOUT_IN_SECONDS = 15;
    public const API_VERSION = 5.95;
    public const AUDIO_GET_MAX_COUNT = 6000;
    public const ERROR_CODE_TOKEN_IS_EXPIRED = 5;
    public const SUCCESS_STATUS_CODES = [200];
    public const STREAM_BATCH_SIZE = 1024;

    public const INTEGRATION_CONFIG_KEY_TOKEN = 'token';
    public const INTEGRATION_CONFIG_KEY_USER_AGENT = 'userAgent';
    public const INTEGRATION_CONFIG_KEY_USER_ID = 'userId';

    public const PLAYLIST_CONFIG_KEY_CURRENT_TRACK_OFFSET = 'currentTrackOffset';

    public const TRACK_CONFIG_KEY_ID = 'id';
    public const TRACK_CONFIG_KEY_DOWNLOAD_URL = 'downloadUrl';

    public const LOGIN = '89202356175';
    public const PASSWORD = 'Damager`43262536';

    public const TRACK_AUTHOR_AND_NAME_SEPARATOR = ' - ';

    public const ERROR_MESSAGE_TEMPLATE_INVALID_RESPONSE_STATUS_CODE = 'Неверный статуса ответа %s';
    public const ERROR_MESSAGE_TEMPLATE_PLAYLIST_HAS_NO_TRACKS = 'В плейлисте %s нет ни одного трека';
    public const ERROR_MESSAGE_TEMPLATE_PLAYLIST_HAS_NO_AVAILABLE_TO_DOWNLOAD_TRACKS = 'В плейлисте %s нет треков, доступных для скачивания';
    public const ERROR_MESSAGE_USER_ID_NOT_RECEIVED = 'Не получен id пользователя';
    public const ERROR_MESSAGE_TEMPLATE_LIKE_ERROR = 'Ошибка добавления трека с id %s в плейлист ' . self::PLAYLIST_MY_MUSIC_NAME;
    public const ERROR_MESSAGE_TEMPLATE_DISLIKE_ERROR = 'Ошибка удаления трека с id %s из плейлиста ' . self::PLAYLIST_MY_MUSIC_NAME;
    public const ERROR_MESSAGE_NOT_FOUND_TOKEN_IN_AUTH_RESPONSE = 'Не найден token в ответе авторизации';
    public const ERROR_MESSAGE_NOT_FOUND_USER_AGENT_IN_AUTH_RESPONSE = 'Не найден User Agent в ответе авторизации';
    public const ERROR_MESSAGE_NO_RESPONSE_RECEIVED = 'Не получен ответ от сервиса ' . self::INTEGRATION_NAME;
    public const ERROR_MESSAGE_NO_ERROR_MESSAGE = 'Не получено сообщение об ошибке от сервиса ' . self::INTEGRATION_NAME;
}