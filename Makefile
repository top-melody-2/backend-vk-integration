default: command

build-container:
	docker build -t tm2-vk-integration:latest ./docker/php
command:
	docker run -v `pwd`:/app -it --name tm2-vk-integration --rm tm2-vk-integration $(c)
composer-install:
	docker run -v `pwd`:/app -it --name tm2-vk-integration --rm -e XDEBUG_MODE=off tm2-vk-integration composer install -n
composer-update-integration-core:
	docker run -v `pwd`:/app -it --name tm2-vk-integration --rm -e XDEBUG_MODE=off tm2-vk-integration composer update top-melody-2/backend-integration-core -n